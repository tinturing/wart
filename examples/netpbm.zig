const std = @import("std");

const wart = @import("wart");

const Img = wart.image.Img;

const Error = error{
    BadHeader,
    Unsupported,
    FileTooShort,
};

pub fn initFromPath(allocator: std.mem.Allocator, path: []const u8) !Img {
    const ext = std.fs.path.extension(path);

    const file = try std.fs.cwd().openFile(
        path,
        .{},
    );
    defer file.close();

    if (std.ascii.eqlIgnoreCase(ext, ".ppm")) {
        return try parseFile(allocator, &file);
    } else {
        return Error.Unsupported;
    }
}

pub fn writeToPath(img: wart.image.ImgRef, path: []const u8) !void {
    const file = try std.fs.cwd().createFile(
        path,
        .{ .read = true },
    );
    defer file.close();
    const writer = file.writer();

    try writer.writeAll("P6\n");
    try writer.print("{d} {d}\n255\n", .{ img.size().width, img.size().height });
    try writer.writeAll(std.mem.sliceAsBytes(img.items()));
}

pub fn parseFile(allocator: std.mem.Allocator, file: *const std.fs.File) !Img {
    const reader = file.reader();

    const header = try Header.parse(reader);

    var img = try Img.init(allocator, header.size);
    errdefer img.deinit();

    const img_size = header.size.width * header.size.height;

    // TODO Do NOT ignore header.MaxValue
    const read_size = try reader.read(std.mem.sliceAsBytes(img.items()));
    if (read_size != img_size * @sizeOf(wart.color.Rgb)) {
        return Error.FileTooShort;
    }

    return img;
}

const Header = struct {
    kind: Kind,
    size: wart.Size,
    max_value: u8,

    const Kind = enum {
        P1,
        P2,
        P3,
        P4,
        P5,
        P6,
    };

    // TODO Handle comments
    pub fn parse(reader: std.fs.File.Reader) !Header {
        if ((try reader.readByte()) != 'P') {
            return Error.BadHeader;
        }

        const kind = switch (try reader.readByte()) {
            '1' => return Error.Unsupported,
            '2' => return Error.Unsupported,
            '3' => return Error.Unsupported,
            '4' => return Error.Unsupported,
            '5' => return Error.Unsupported,
            '6' => Kind.P6,
            else => return Error.Unsupported,
        };

        if (!std.ascii.isSpace(try reader.readByte())) {
            return Error.BadHeader;
        }

        var buf: [16]u8 = undefined;

        const width_buf = (try reader.readUntilDelimiterOrEof(buf[0..], ' ')) orelse
            return Error.BadHeader;

        const width = std.fmt.parseInt(usize, width_buf, 10) catch
            return Error.BadHeader;

        const height_buf = (try reader.readUntilDelimiterOrEof(buf[0..], '\n')) orelse
            return Error.BadHeader;

        const height = std.fmt.parseInt(usize, height_buf, 10) catch
            return Error.BadHeader;

        const max_value_buf = (try reader.readUntilDelimiterOrEof(buf[0..], '\n')) orelse
            return Error.BadHeader;

        const max_value = std.fmt.parseInt(u8, max_value_buf, 10) catch
            return Error.BadHeader;

        return Header{
            .kind = kind,
            .size = .{ .width = width, .height = height },
            .max_value = max_value,
        };
    }
};
