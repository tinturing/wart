const std = @import("std");
const builtin = @import("builtin");
const wart = @import("wart");
const image = wart.image;
const Buffer = wart.Buffer;
const color = wart.color;

const netpbm = @import("netpbm.zig");

const Watch = struct {
    last: i128,
    print: bool = true,

    const stderr = std.io.getStdErr().writer();

    pub fn init(print: bool) Watch {
        return Watch{
            .last = std.time.nanoTimestamp(),
            .print = print,
        };
    }

    pub fn lap(self: *Watch, comptime label: []const u8) void {
        const now = std.time.nanoTimestamp();
        const diff = @divTrunc((now - self.last), 1_000_000);
        self.last = now;

        if (self.print) {
            stderr.print("{s}: {d} ms\n", .{ label, diff }) catch return;
        }
    }
};

fn correctionRatio() !f32 {
    const size = try wart.term.size();
    const size_px = try wart.term.sizeInPixels();

    return (@intToFloat(f32, size.height) / @intToFloat(f32, size.width)) *
        (@intToFloat(f32, size_px.width) / @intToFloat(f32, size_px.height));
}

const fallback_size = wart.Size{ .width = 96, .height = 32 };

fn bestFit(img_size: wart.Size) !wart.Size {
    const term_size = wart.term.size() catch fallback_size;

    const ratio = std.math.min(
        @intToFloat(f32, term_size.width) / @intToFloat(f32, img_size.width),
        @intToFloat(f32, term_size.height) / @intToFloat(f32, img_size.height),
    );

    return wart.Size{
        .width = @floatToInt(usize, @intToFloat(f32, img_size.width) * ratio),
        .height = @floatToInt(usize, @intToFloat(f32, img_size.height) * ratio),
    };
}

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.detectLeaks();
    var allocator = gpa.allocator();

    var args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len < 3) {
        std.log.err("expected a CLI argument", .{});
        std.log.err("usage: zig build show -- <typesetter> <image>", .{});
        return;
    }

    var watch = Watch.init(false);

    const ppm = try netpbm.initFromPath(allocator, args[2]);
    defer ppm.deinit();

    watch.lap("opening");

    const typesetter: wart.Typesetter = if (std.mem.eql(u8, args[1], "half"))
        wart.typesetters.half()
    else if (std.mem.eql(u8, args[1], "block"))
        wart.typesetters.block()
    else if (std.mem.eql(u8, args[1], "quadrant"))
        wart.typesetters.quadrant()
    else if (std.mem.eql(u8, args[1], "sextant"))
        wart.typesetters.sextant()
    else if (std.mem.eql(u8, args[1], "asymmetric"))
        wart.typesetters.asymmetric()
    else {
        std.log.err("unknown typesetter", .{});
        std.log.err("available typesetters are: half, quadrant, sextant, and asymmetric", .{});
        std.log.err("usage: zig build show -- <typesetter> <image>", .{});
        return;
    };

    const ratio = correctionRatio() catch 0.46667;
    const img_size = typesetter.correct(typesetter.sizeAsGlyphs(ppm.size()), ratio);
    var buffer_size = try bestFit(img_size);

    var buffer = try Buffer.init(allocator, buffer_size);
    defer buffer.deinit();

    const size = typesetter.sizeAsImage(buffer.glyphs().size());

    const resized = try image.resize(allocator, ppm.ref(), size);
    defer resized.deinit();

    watch.lap("resizing");

    try typesetter.compose(buffer.glyphs(), resized.refConst());

    watch.lap("converting");

    const chars = try buffer.render();

    watch.lap("rendering");

    const out = std.io.getStdOut();
    try out.writeAll(chars);

    watch.lap("printing");
}
