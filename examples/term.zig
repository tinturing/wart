const std = @import("std");

const wart = @import("wart");
const term = wart.term;

pub fn main() anyerror!void {
    const stdout = std.io.getStdOut().writer();

    try stdout.print("Chars {any}\n", .{term.size()});
    try stdout.print("Pixels {any}\n", .{term.sizeInPixels()});

    try stdout.writeAll("\nPress any key. Press q, ^C, or Esc to quit.\n");

    const raw = try term.Raw.init();
    defer raw.deinit();

    const fps = 3;
    const sleep_time = std.time.ns_per_s / fps;

    var keys = term.KeysBuffer(32).init();

    loop: while (true) {
        try keys.read();

        try stdout.print("{any} ", .{keys.raw()});

        var iter = keys.iter();
        while (iter.next()) |key| {
            switch (key) {
                .char => |ch| switch (ch) {
                    'q', 3 => break :loop,
                    else => try stdout.print("{u}, ", .{ch}),
                },
                .up => try stdout.writeAll("Up, "),
                .down => try stdout.writeAll("Down, "),
                .left => try stdout.writeAll("Left, "),
                .right => try stdout.writeAll("Right, "),
                .esc => break :loop,
            }
        }

        try stdout.writeAll("\r\n");

        std.time.sleep(sleep_time);
    }
}
