const std = @import("std");
const wart = @import("wart");
const color = wart.color;

const stdout = std.io.getStdOut().writer();

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.detectLeaks();
    const allocator = gpa.allocator();

    var args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len < 3) {
        std.log.err("expected a CLI argument", .{});
        std.log.err("usage: zig build bench -- <buffer-size> <repeats>", .{});
        return;
    }

    const sz = try std.fmt.parseInt(usize, args[1], 10);
    const repeats = try std.fmt.parseInt(usize, args[2], 10);

    const size = wart.Size{ .width = sz, .height = sz };
    const print = false;
    const random_config = .{ .size = size, .repeats = repeats, .print = print, .random = true };
    const static_config = .{ .size = size, .repeats = repeats, .print = print, .random = false };

    try bench(wart.DoubleBuffer, allocator, random_config);
    try bench(wart.Buffer, allocator, random_config);
    try bench(wart.DoubleBuffer, allocator, static_config);
    try bench(wart.Buffer, allocator, static_config);
}

const Config = struct {
    size: wart.Size,
    repeats: usize,
    print: bool,
    random: bool,
};

fn pause() !void {
    var buf: [1]u8 = undefined;

    try stdout.writeAll("Press enter to continue...\n");
    _ = try std.io.getStdIn().read(buf[0..]);
}

fn bench(
    comptime Buffer: type,
    allocator: std.mem.Allocator,
    config: Config,
) !void {
    if (config.print) {
        try stdout.writeAll("\x1b[2J");
    }

    var buffer = try Buffer.init(allocator, config.size);
    defer buffer.deinit();

    var fill_times: i128 = 0;
    var render_times: i128 = 0;
    var print_times: i128 = 0;
    var char_count: u128 = 0;

    var rnd = std.rand.DefaultPrng.init(1541354234);

    var frames: usize = 0;
    while (frames < config.repeats) : (frames += 1) {
        const start = std.time.nanoTimestamp();

        try fill(Buffer, &buffer, if (config.random) rnd.random() else null);

        const after_fill = std.time.nanoTimestamp();
        fill_times += after_fill - start;

        const chars = try buffer.render();

        char_count += chars.len;

        const after_render = std.time.nanoTimestamp();
        render_times += after_render - after_fill;

        if (config.print) {
            try stdout.writeAll("\x1b[1;1H");
            try stdout.writeAll(chars);

            const after_print = std.time.nanoTimestamp();
            print_times += after_print - after_render;
        }
    }

    try stdout.print("{}, random: {}\n", .{ Buffer, config.random });
    try stdout.print("chars: {}\n", .{char_count});
    try stdout.print("fill:   {d:12} ns/frame\n", .{@divTrunc(fill_times, frames)});
    try stdout.print("render: {d:12} ns/frame\n", .{@divTrunc(render_times, frames)});

    if (config.print) {
        try stdout.print("print:  {d:12} ns/frame\n", .{@divTrunc(print_times, frames)});
        try pause();
    }

    try stdout.writeAll("\n");
}

fn fill(
    comptime Buffer: type,
    buffer: *Buffer,
    rnd: ?std.rand.Random,
) !void {
    var buf: [6]u8 = [_]u8{ 32, 32, 32, 64, 64, 64 };

    const codepoint: u21 = '.';

    var rows = buffer.glyphs().rows();
    while (rows.next()) |row| {
        for (row) |*glyph| {
            if (rnd) |r| {
                r.bytes(buf[0..]);
            }

            var fg = wart.color.Rgb.init(buf[0], buf[1], buf[2]);
            var bg = wart.color.Rgb.init(buf[3], buf[4], buf[5]);

            glyph.codepoint = if (rnd) |r|
                r.intRangeLessThan(u21, 0x20, 0x7e)
            else
                codepoint;

            glyph.foreground = fg;
            glyph.background = bg;
        }
    }
}
