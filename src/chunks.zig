/// An iterator over a slice in chunks of size `chunk_size`.
///
/// The remainder will be omitted, if the slice is not evenly divisible by
/// `chunk_size`.
pub fn Chunks(comptime T: type) type {
    return ChunksBase(T, .mutable);
}

pub fn ChunksConst(comptime T: type) type {
    return ChunksBase(T, .constant);
}

const ChunksModifier = enum { mutable, constant };

fn ChunksBase(comptime T: type, comptime modifier: ChunksModifier) type {
    const SliceType = switch (modifier) {
        .mutable => []T,
        .constant => []const T,
    };

    return struct {
        slice: SliceType,
        chunk_size: usize,

        const Self = @This();

        pub fn init(slice: SliceType, chunk_size: usize) Self {
            return Self{
                .slice = slice,
                .chunk_size = chunk_size,
            };
        }

        pub fn next(self: *Self) ?SliceType {
            if (self.slice.len < self.chunk_size) {
                return null;
            } else {
                defer self.slice = self.slice[self.chunk_size..];
                return self.slice[0..self.chunk_size];
            }
        }
    };
}
