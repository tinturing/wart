const rectangle = @import("rectangle.zig");
const Typesetter = @import("../Typesetter.zig");

pub fn init() Typesetter {
    return rectangle.init(
        glyph_size,
        findBestTemplate,
    );
}

const glyph_size = .{ .width = 4, .height = 8 };

const Template = rectangle.Template(glyph_size);

fn findBestTemplate(pattern: u32) *const Template {
    var best_so_far: *const Template = &default;
    var smallest_diff: u32 = 32;

    return for (templates) |*template| {
        if (pattern == template.pattern) {
            break template;
        } else {
            const diff = @popCount(template.pattern ^ pattern);
            if (diff < smallest_diff) {
                best_so_far = template;
                smallest_diff = diff;
            }
        }
    } else best_so_far;
}

const default = Template.init(' ', 0x0000_0000, false);

const templates = [_]Template{
    Template.init(' ', 0x0000_0000, false),
    Template.init('▁', 0x0000_000f, false),
    Template.init('▂', 0x0000_00ff, false),
    Template.init('▃', 0x0000_0fff, false),
    Template.init('▄', 0x0000_ffff, false),
    Template.init('▅', 0x000f_ffff, false),
    Template.init('▆', 0x00ff_ffff, false),
    Template.init('▇', 0x0fff_ffff, false),
    Template.init(' ', 0xffff_ffff, true),
    Template.init('▁', 0xffff_fff0, true),
    Template.init('▂', 0xffff_ff00, true),
    Template.init('▃', 0xffff_f000, true),
    Template.init('▄', 0xffff_0000, true),
    Template.init('▅', 0xfff0_0000, true),
    Template.init('▆', 0xff00_0000, true),
    Template.init('▇', 0xf000_0000, true),
    Template.init('▊', 0xeeee_eeee, false),
    Template.init('▌', 0xcccc_cccc, false),
    Template.init('▎', 0x8888_8888, false),
    Template.init('▎', 0x7777_7777, true),
    Template.init('▌', 0x3333_3333, true),
    Template.init('▊', 0x1111_1111, true),
    Template.init('▗', 0x0000_3333, false),
    Template.init('▖', 0x0000_cccc, false),
    Template.init('▝', 0x3333_0000, false),
    Template.init('▘', 0xcccc_0000, false),
    Template.init('▞', 0x3333_cccc, false),
    Template.init('▚', 0xcccc_3333, false),
    Template.init('▟', 0x3333_ffff, false),
    Template.init('▙', 0xcccc_ffff, false),
    Template.init('▜', 0xffff_3333, false),
    Template.init('▛', 0xffff_cccc, false),
    Template.init('■', 0x0006_6000, false),
    Template.init('■', 0xfff9_9fff, true),
    Template.init('▬', 0x000f_f000, false),
    Template.init('━', 0x000f_0000, false),
    Template.init('━', 0x0000_f000, false),
    Template.init('▬', 0xfff0_0fff, true),
    Template.init('━', 0xfff0_ffff, true),
    Template.init('━', 0xffff_0fff, true),
    Template.init('┃', 0x6666_6666, false),
    Template.init('│', 0x4444_4444, false),
    Template.init('│', 0x2222_2222, false),
    Template.init('┃', 0x9999_9999, true),
    Template.init('│', 0xdddd_dddd, true),
    Template.init('│', 0xbbbb_bbbb, true),
    Template.init('╸', 0x000c_c000, false),
    Template.init('╺', 0x0003_3000, false),
    Template.init('╹', 0x6666_0000, false),
    Template.init('╻', 0x0000_6666, false),
};
