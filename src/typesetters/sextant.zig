const rectangle = @import("rectangle.zig");
const Typesetter = @import("../Typesetter.zig");

pub fn init() Typesetter {
    return rectangle.init(
        glyph_size,
        findBestTemplate,
    );
}

const glyph_size = .{ .width = 2, .height = 3 };

const Template = rectangle.Template(glyph_size);

fn findBestTemplate(pattern: u6) *const Template {
    return &templates[pattern];
}

const templates = [_]Template{
    Template.init(' ', 0b00_00_00, false),
    Template.init('🬞', 0b00_00_01, false),
    Template.init('🬏', 0b00_00_10, false),
    Template.init('🬭', 0b00_00_11, false),
    Template.init('🬇', 0b00_01_00, false),
    Template.init('🬦', 0b00_01_01, false),
    Template.init('🬖', 0b00_01_10, false),
    Template.init('🬵', 0b00_01_11, false),
    Template.init('🬃', 0b00_10_00, false),
    Template.init('🬢', 0b00_10_01, false),
    Template.init('🬓', 0b00_10_10, false),
    Template.init('🬱', 0b00_10_11, false),
    Template.init('🬋', 0b00_11_00, false),
    Template.init('🬩', 0b00_11_01, false),
    Template.init('🬚', 0b00_11_10, false),
    Template.init('🬹', 0b00_11_11, false),
    Template.init('🬁', 0b01_00_00, false),
    Template.init('🬠', 0b01_00_01, false),
    Template.init('🬑', 0b01_00_10, false),
    Template.init('🬯', 0b01_00_11, false),
    Template.init('🬉', 0b01_01_00, false),
    Template.init('▐', 0b01_01_01, false),
    Template.init('🬘', 0b01_01_10, false),
    Template.init('🬷', 0b01_01_11, false),
    Template.init('🬅', 0b01_10_00, false),
    Template.init('🬤', 0b01_10_01, false),
    Template.init('🬔', 0b01_10_10, false),
    Template.init('🬳', 0b01_10_11, false),
    Template.init('🬍', 0b01_11_00, false),
    Template.init('🬫', 0b01_11_01, false),
    Template.init('🬜', 0b01_11_10, false),
    Template.init('🬻', 0b01_11_11, false),
    Template.init('🬀', 0b10_00_00, false),
    Template.init('🬟', 0b10_00_01, false),
    Template.init('🬐', 0b10_00_10, false),
    Template.init('🬮', 0b10_00_11, false),
    Template.init('🬈', 0b10_01_00, false),
    Template.init('🬧', 0b10_01_01, false),
    Template.init('🬗', 0b10_01_10, false),
    Template.init('🬶', 0b10_01_11, false),
    Template.init('🬄', 0b10_10_00, false),
    Template.init('🬣', 0b10_10_01, false),
    Template.init('▌', 0b10_10_10, false),
    Template.init('🬲', 0b10_10_11, false),
    Template.init('🬌', 0b10_11_00, false),
    Template.init('🬪', 0b10_11_01, false),
    Template.init('🬛', 0b10_11_10, false),
    Template.init('🬺', 0b10_11_11, false),
    Template.init('🬂', 0b11_00_00, false),
    Template.init('🬡', 0b11_00_01, false),
    Template.init('🬒', 0b11_00_10, false),
    Template.init('🬰', 0b11_00_11, false),
    Template.init('🬊', 0b11_01_00, false),
    Template.init('🬨', 0b11_01_01, false),
    Template.init('🬙', 0b11_01_10, false),
    Template.init('🬸', 0b11_01_11, false),
    Template.init('🬆', 0b11_10_00, false),
    Template.init('🬥', 0b11_10_01, false),
    Template.init('🬕', 0b11_10_10, false),
    Template.init('🬴', 0b11_10_11, false),
    Template.init('🬎', 0b11_11_00, false),
    Template.init('🬬', 0b11_11_01, false),
    Template.init('🬝', 0b11_11_10, false),
    Template.init(' ', 0b11_11_11, true),
};
