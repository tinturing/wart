const std = @import("std");
const glyph = @import("../glyph.zig");
const color = @import("../color.zig");
const Size = @import("../matrix.zig").Size;
const Typesetter = @import("../Typesetter.zig");
const GlyphsRef = @import("../buffer.zig").GlyphsRef;
const ImgRefConst = @import("../image.zig").ImgRefConst;

/// Unsigned integer used as the type of the 'pattern' of a single Glyph.
fn Uint(comptime size: Size) type {
    // NOTE As a special exception the `block` typesetter uses `u2` instead of the
    // expected `u1` pattern type. Otherwise, the glyphing algorithm would run
    // into some issues with some bit shifts.
    if (size.width == 1 and size.height == 1) {
        return u2;
    }

    return std.meta.Int(.unsigned, size.width * size.height);
}

pub fn init(
    comptime glyph_size: Size,
    comptime findBestTemplateFn: fn (Uint(glyph_size)) *const Template(glyph_size),
) Typesetter {
    const impl = struct {
        const CellItem = struct {
            gray: u8,
            color: color.Rgb,
        };

        pub fn compose(glyphs: GlyphsRef, image: ImgRefConst) void {
            const pixels = image.items();
            const px_width = image.size().width;

            const cell_size = glyph_size.width * glyph_size.height;
            var cell: [cell_size]CellItem = undefined;

            const in_width = px_width / glyph_size.width;
            const out_width = std.math.min(in_width, glyphs.size().width);

            const in_height = image.size().height / glyph_size.height;
            const out_height = std.math.min(in_height, glyphs.size().height);

            var row: usize = 0;
            while (row < out_height) : (row += 1) {
                var col: usize = 0;
                while (col < out_width) : (col += 1) {
                    var gray_sum: u16 = 0;

                    const col_rgb = col * glyph_size.width;
                    const row_rgb = row * glyph_size.height;

                    var i: usize = 0;
                    while (i < glyph_size.height) : (i += 1) {
                        var j: usize = 0;
                        while (j < glyph_size.width) : (j += 1) {
                            const px = pixels[(row_rgb + i) * px_width + (col_rgb + j)];
                            const gray = px.luminosity();

                            gray_sum += gray;

                            cell[i * glyph_size.width + j].gray = gray;
                            cell[i * glyph_size.width + j].color = px;
                        }
                    }

                    var pattern: Uint(glyph_size) = 0;
                    const gray_avg = gray_sum / cell.len;

                    for (cell) |*item| {
                        pattern <<= 1;
                        if (item.gray > gray_avg) {
                            pattern |= 1;
                        }
                    }

                    const template = findBestTemplateFn(pattern);

                    var fg_avg = color.Average{};
                    var bg_avg = color.Average{};

                    var mask: Uint(glyph_size) = 1 << ((glyph_size.width * glyph_size.height) - 1);
                    for (cell) |*item| {
                        if ((template.pattern & mask) == 0) {
                            bg_avg.add(item.color);
                        } else {
                            fg_avg.add(item.color);
                        }
                        mask >>= 1;
                    }

                    const fg = if (template.is_inverted) bg_avg.get() else fg_avg.get();
                    const bg = if (template.is_inverted) fg_avg.get() else bg_avg.get();

                    glyphs.items()[row * glyphs.size().width + col] = glyph.Glyph{
                        .codepoint = template.codepoint,
                        .foreground = fg,
                        .background = bg,
                    };
                }
            }
        }
    };

    return Typesetter{
        .glyph_size = glyph_size,
        .composeFn = impl.compose,
    };
}

pub fn Template(comptime size: Size) type {
    return struct {
        codepoint: glyph.Glyph.Codepoint,
        pattern: Uint(size),
        is_inverted: bool,

        pub fn init(
            codepoint: glyph.Glyph.Codepoint,
            pattern: Uint(size),
            is_inverted: bool,
        ) Template(size) {
            return .{
                .codepoint = codepoint,
                .pattern = pattern,
                .is_inverted = is_inverted,
            };
        }
    };
}
