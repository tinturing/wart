const rectangle = @import("rectangle.zig");
const Typesetter = @import("../Typesetter.zig");

pub fn init() Typesetter {
    return rectangle.init(
        glyph_size,
        findBestTemplate,
    );
}

const glyph_size = .{ .width = 2, .height = 2 };

const Template = rectangle.Template(glyph_size);

fn findBestTemplate(pattern: u4) *const Template {
    return &templates[pattern];
}

const templates = [_]Template{
    Template.init(' ', 0b0000, false),
    Template.init('▗', 0b0001, false),
    Template.init('▖', 0b0010, false),
    Template.init('▄', 0b0011, false),
    Template.init('▝', 0b0100, false),
    Template.init('▐', 0b0101, false),
    Template.init('▞', 0b0110, false),
    Template.init('▟', 0b0111, false),

    Template.init('▘', 0b1000, false),
    Template.init('▚', 0b1001, false),
    Template.init('▌', 0b1010, false),
    Template.init('▙', 0b1011, false),
    Template.init('▀', 0b1100, false),
    Template.init('▜', 0b1101, false),
    Template.init('▛', 0b1110, false),
    Template.init(' ', 0b1111, true),
};
