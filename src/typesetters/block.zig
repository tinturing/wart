const rectangle = @import("rectangle.zig");
const Typesetter = @import("../Typesetter.zig");

pub fn init() Typesetter {
    return rectangle.init(
        glyph_size,
        findBestTemplate,
    );
}

const glyph_size = .{ .width = 1, .height = 1 };

const Template = rectangle.Template(glyph_size);

// NOTE As a special exception the `block` typesetter uses `u2` instead of the
// expected `u1` pattern type. Otherwise, the glyphing algorithm would run into
// some issues with some bit shifts.
fn findBestTemplate(_: u2) *const Template {
    return &template;
}

const template = Template.init(' ', 0b0, false);
