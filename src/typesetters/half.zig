const rectangle = @import("rectangle.zig");
const Typesetter = @import("../Typesetter.zig");

pub fn init() Typesetter {
    return rectangle.init(
        glyph_size,
        findBestTemplate,
    );
}

const glyph_size = .{ .width = 1, .height = 2 };

const Template = rectangle.Template(glyph_size);

fn findBestTemplate(pattern: u2) *const Template {
    return &templates[pattern];
}

const templates = [_]Template{
    Template.init(' ', 0b00, false),
    Template.init('▄', 0b01, false),
    Template.init('▀', 0b10, false),
    Template.init(' ', 0b11, true),
};
