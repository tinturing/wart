const std = @import("std");

const matrix = @import("matrix.zig");

pub fn size() !matrix.Size {
    const winsize = try getWinsize();
    return matrix.Size{ .width = winsize.ws_col, .height = winsize.ws_row };
}

pub fn sizeInPixels() !matrix.Size {
    const winsize = try getWinsize();
    return matrix.Size{ .width = winsize.ws_xpixel, .height = winsize.ws_ypixel };
}

fn getWinsize() !std.os.linux.winsize {
    var winsize: std.os.linux.winsize = undefined;

    const ret = std.os.linux.ioctl(
        std.io.getStdOut().handle,
        std.os.linux.T.IOCGWINSZ,
        @ptrToInt(&winsize),
    );

    return if (ret == 0) winsize else error.NoTerminalSize;
}

pub const Raw = struct {
    fd: std.fs.File.Handle,
    original: std.os.termios,

    pub fn init() !Raw {
        return initWithFile(std.io.getStdOut());
    }

    fn initWithFile(file: std.fs.File) !Raw {
        const fd = file.handle;

        var termios = try std.os.tcgetattr(fd);
        const original = termios;

        const l = std.os.linux;

        // https://man7.org/linux/man-pages/man3/cfmakeraw.3.html
        termios.iflag &= ~(l.IGNBRK | l.BRKINT | l.PARMRK | l.ISTRIP | l.INLCR | l.IGNCR | l.ICRNL | l.IXON);
        termios.oflag &= ~l.OPOST;
        termios.lflag &= ~(l.ECHO | l.ECHONL | l.ICANON | l.ISIG | l.IEXTEN);
        termios.cflag &= ~(l.CSIZE | l.PARENB);
        termios.cflag |= l.CS8;

        termios.cc[l.V.MIN] = 0;
        termios.cc[l.V.TIME] = 0;

        try std.os.tcsetattr(fd, std.os.TCSA.FLUSH, termios);

        return Raw{
            .fd = fd,
            .original = original,
        };
    }

    pub fn deinit(self: *const Raw) void {
        std.os.tcsetattr(self.fd, std.os.TCSA.FLUSH, self.original) catch {};
    }
};

pub const Key = union(enum) {
    up,
    down,
    left,
    right,
    esc,
    char: u8,
};

pub fn KeysBuffer(comptime len: usize) type {
    return struct {
        buffer: [len]u8 = undefined,
        count: usize = 0,

        const Self = @This();
        const stdin = std.io.getStdIn();

        pub fn init() Self {
            return Self{};
        }

        pub fn read(self: *Self) !void {
            self.count = try stdin.read(self.buffer[0..]);
        }

        pub fn raw(self: *const Self) []const u8 {
            return self.buffer[0..self.count];
        }

        pub fn iter(self: *Self) Keys {
            return Keys.init(self.raw());
        }
    };
}

pub const Keys = struct {
    slice: []const u8,

    const Self = @This();

    pub fn init(slice: []const u8) Self {
        return Self{
            .slice = slice,
        };
    }

    pub fn next(self: *Self) ?Key {
        if (self.slice.len == 0) {
            return null;
        } else if (self.slice[0] == '\x1b') {
            if (self.slice.len > 2 and self.slice[1] == '[') {
                return switch (self.slice[2]) {
                    'A' => self.consume(3, .up),
                    'B' => self.consume(3, .down),
                    'C' => self.consume(3, .right),
                    'D' => self.consume(3, .left),
                    else => self.consume(1, .esc),
                };
            }

            return self.consume(1, .esc);
        } else {
            return self.consume(1, Key{ .char = self.slice[0] });
        }
    }

    fn consume(self: *Self, advance: usize, key: Key) ?Key {
        self.slice = self.slice[advance..];
        return key;
    }
};
