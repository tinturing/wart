const color = @import("color.zig");

/// A Unicode codepoint with a foreground and a background color.
pub const Glyph = struct {
    codepoint: Codepoint,
    foreground: ?color.Rgb,
    background: ?color.Rgb,

    /// A Unicode codepoint.
    pub const Codepoint = u21;

    pub fn init(
        codepoint: Codepoint,
        foreground: color.Rgb,
        background: color.Rgb,
    ) Glyph {
        return Glyph{
            .codepoint = codepoint,
            .foreground = foreground,
            .background = background,
        };
    }
};
