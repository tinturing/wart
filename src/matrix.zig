const std = @import("std");
const testing = std.testing;
const chunks = @import("chunks.zig");

pub const Error = error{
    InvalidSize,
    OutOfBounds,
};

pub const Size = struct {
    width: usize,
    height: usize,
};

pub fn Matrix(comptime T: type) type {
    return struct {
        allocator: std.mem.Allocator,
        _ref: MatrixRef(T),

        const Self = @This();

        pub fn init(allocator: std.mem.Allocator, sz: Size) !Self {
            const slice = try allocator.alloc(T, sz.width * sz.height);

            return Self{
                .allocator = allocator,
                ._ref = try MatrixRef(T).init(slice, sz),
            };
        }

        pub fn deinit(self: *const Self) void {
            self.allocator.free(self._ref.items());
        }

        pub fn items(self: *const Self) []T {
            return self._ref.items();
        }

        pub fn size(self: *const Self) Size {
            return self._ref.size();
        }

        pub usingnamespace MatrixBase(Self, T, items, size);
        pub usingnamespace MatrixConstBase(Self, T, items, size);
    };
}

pub fn MatrixRef(comptime T: type) type {
    return struct {
        _items: []T,
        _size: Size,

        const Self = @This();

        pub fn init(slice: []T, sz: Size) Error!Self {
            if (slice.len != sz.width * sz.height) {
                return Error.InvalidSize;
            }

            return Self{
                ._items = slice,
                ._size = sz,
            };
        }

        pub fn items(self: *const Self) []T {
            return self._items;
        }

        pub fn size(self: *const Self) Size {
            return self._size;
        }

        pub usingnamespace MatrixBase(Self, T, items, size);
        pub usingnamespace MatrixConstBase(Self, T, items, size);
    };
}

pub fn MatrixRefConst(comptime T: type) type {
    return struct {
        _items: []const T,
        _size: Size,

        const Self = @This();

        pub fn init(slice: []const T, sz: Size) Error!Self {
            if (slice.len != sz.width * sz.height) {
                return Error.InvalidSize;
            }

            return Self{
                ._items = slice,
                ._size = sz,
            };
        }

        pub fn items(self: *const Self) []const T {
            return self._items;
        }

        pub fn size(self: *const Self) Size {
            return self._size;
        }

        pub usingnamespace MatrixConstBase(Self, T, items, size);
    };
}

fn IndexFn(
    comptime Self: type,
    comptime size: fn (*const Self) Size,
) type {
    return struct {
        pub fn index(self: *const Self, x: usize, y: usize) Error!usize {
            return if (x >= size(self).width or y >= size(self).height)
                Error.OutOfBounds
            else
                y * size(self).width + x;
        }
    };
}

pub fn Split(comptime T: type) type {
    return std.meta.Tuple(&.{ MatrixRef(T), MatrixRef(T) });
}

fn MatrixBase(
    comptime Self: type,
    comptime T: type,
    comptime items: fn (*const Self) []T,
    comptime size: fn (*const Self) Size,
) type {
    return struct {
        // TODO Work out pointer mutability. A const pointer works here because
        // `items` returns a mutable slice, but a const pointer seems like the
        // wrong choice for `self`, since `set` mutates `self`... sort of.
        pub fn set(self: *const Self, x: usize, y: usize, value: T) Error!void {
            const ptr = try getPtr(self, x, y);
            ptr.* = value;
        }

        pub fn getPtr(self: *const Self, x: usize, y: usize) Error!*T {
            return &items(self)[try self.index(x, y)];
        }

        pub fn rows(self: *const Self) chunks.Chunks(T) {
            return chunks.Chunks(T).init(items(self), size(self).width);
        }

        pub fn ref(self: *const Self) MatrixRef(T) {
            // NOTE Bypassing `MatrixRef(T).init` is safe if `self` has been
            // created by a `fn init` and has not been invalidated by modifying
            // its fields. Users should NOT manually use the `Matrix_`
            // initializers but use `fn init`, instead.
            return MatrixRef(T){
                ._items = items(self),
                ._size = size(self),
            };
        }

        /// Splits `self` into two `MatrixRef`s at the specified `row`.
        pub fn split(
            self: *const Self,
            row: usize,
        ) !Split(T) {
            if (row > size(self).height) {
                return Error.OutOfBounds;
            }

            const pos = row * size(self).width;

            return .{
                try MatrixRef(T).init(
                    items(self)[0..pos],
                    .{
                        .width = size(self).width,
                        .height = row,
                    },
                ),
                try MatrixRef(T).init(
                    items(self)[pos..],
                    .{
                        .width = size(self).width,
                        .height = size(self).height - row,
                    },
                ),
            };
        }

        usingnamespace IndexFn(Self, size);
    };
}

pub fn SplitConst(comptime T: type) type {
    return std.meta.Tuple(&.{ MatrixRefConst(T), MatrixRefConst(T) });
}

fn MatrixConstBase(
    comptime Self: type,
    comptime T: type,
    comptime items: fn (*const Self) []const T,
    comptime size: fn (*const Self) Size,
) type {
    return struct {
        pub fn get(self: *const Self, x: usize, y: usize) Error!T {
            const ptr = try getPtrConst(self, x, y);
            return ptr.*;
        }

        pub fn getPtrConst(self: *const Self, x: usize, y: usize) Error!*const T {
            return &items(self)[try self.index(x, y)];
        }

        pub fn promote(self: *const Self, allocator: std.mem.Allocator) !Matrix(T) {
            const promoted = try Matrix(T).init(allocator, size(self));

            std.mem.copy(T, promoted.items(), items(self));

            return promoted;
        }

        pub fn refConst(self: *const Self) MatrixRefConst(T) {
            // NOTE Bypassing `MatrixRef(T).init` is safe if `self` has been
            // created by a `fn init` and has not been invalidated by modifying
            // its fields. Users should NOT manually use the `Matrix_`
            // initializers but use `fn init`, instead.
            return MatrixRefConst(T){
                ._items = items(self),
                ._size = size(self),
            };
        }

        pub fn rowsConst(self: *const Self) chunks.ChunksConst(T) {
            return chunks.ChunksConst(T).init(items(self), size(self).width);
        }

        /// Splits `self` into two `MatrixRefConst`s at the specified `row`.
        pub fn splitConst(
            self: *const Self,
            row: usize,
        ) !SplitConst(T) {
            if (row > size(self).height) {
                return Error.OutOfBounds;
            }

            const pos = row * size(self).width;

            return .{
                try MatrixRefConst(T).init(
                    items(self)[0..pos],
                    .{
                        .width = size(self).width,
                        .height = row,
                    },
                ),
                try MatrixRefConst(T).init(
                    items(self)[pos..],
                    .{
                        .width = size(self).width,
                        .height = size(self).height - row,
                    },
                ),
            };
        }

        pub fn map(
            self: *const Self,
            comptime R: type,
            allocator: std.mem.Allocator,
            f: *const fn (T) R,
        ) !Matrix(R) {
            var dest = try Matrix(R).init(allocator, size(self));
            errdefer dest.deinit();

            const rs = dest.items();
            for (items(self)) |t, i| {
                rs[i] = f(t);
            }

            return dest;
        }

        usingnamespace IndexFn(Self, size);
    };
}
