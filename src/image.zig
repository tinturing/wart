const std = @import("std");
const color = @import("color.zig");
const Size = matrix.Size;
const matrix = @import("matrix.zig");

pub const ImgRefConst = matrix.MatrixRefConst(color.Rgb);
pub const ImgRef = matrix.MatrixRef(color.Rgb);
pub const Img = matrix.Matrix(color.Rgb);

pub const PixelCastError = error{
    Misaligned,
};

fn reinterpret(comptime T: type, raw: []u8) PixelCastError![]T {
    if (raw.len % @sizeOf(T) != 0) {
        return PixelCastError.Misaligned;
    }
    return std.mem.bytesAsSlice(T, raw);
}

pub fn bytesAsRgb(raw: []u8) PixelCastError![]color.Rgb {
    return reinterpret(color.Rgb, raw);
}

pub fn resize(allocator: std.mem.Allocator, self: ImgRef, size: Size) !Img {
    var new = try Img.init(allocator, size);
    errdefer new.deinit();

    const w_ratio: f32 = @intToFloat(f32, self.size().width) / @intToFloat(f32, size.width);
    const h_ratio: f32 = @intToFloat(f32, self.size().height) / @intToFloat(f32, size.height);

    var row: usize = 0;
    while (row < size.height) : (row += 1) {
        const old_row = @floatToInt(usize, std.math.floor(@intToFloat(f32, row) * h_ratio));

        var col: usize = 0;
        while (col < size.width) : (col += 1) {
            const old_col = @floatToInt(usize, std.math.floor(@intToFloat(f32, col) * w_ratio));

            new.items()[(row * size.width) + col] = self.items()[(old_row * self.size().width) + old_col];
        }
    }

    return new;
}
