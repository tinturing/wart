const std = @import("std");
const GlyphsRef = @import("buffer.zig").GlyphsRef;
const ImgRefConst = @import("image.zig").ImgRefConst;
const color = @import("color.zig");
const Size = matrix.Size;
const Glyph = @import("glyph.zig").Glyph;
const Typesetter = @This();
const matrix = @import("matrix.zig");

glyph_size: Size,
composeFn: *const fn (glyphs: GlyphsRef, image: ImgRefConst) void,

pub fn compose(
    self: *const Typesetter,
    glyphs: GlyphsRef,
    image: ImgRefConst,
) !void {
    self.composeFn(glyphs, image);
}

pub fn composeParallel(
    self: *const Typesetter,
    allocator: std.mem.Allocator,
    glyphs: GlyphsRef,
    image: ImgRefConst,
    jobs: usize,
) !void {
    const cpu_count = if (jobs == 0) try std.Thread.getCpuCount() else jobs;

    var iter = self.workloads(glyphs, image, cpu_count);

    var threads = try std.ArrayList(std.Thread).initCapacity(allocator, cpu_count);
    defer threads.deinit();

    while (try iter.next()) |workload| {
        try threads.append(try std.Thread.spawn(.{}, compose, .{ self, workload.glyphs, workload.image }));
    }

    for (threads.items) |thread| {
        thread.join();
    }
}

/// Returns the `Size` an image has to have to fit the `glyphs`.
pub fn sizeAsImage(self: *const Typesetter, glyphs_size: Size) Size {
    return Size{
        .width = glyphs_size.width * self.glyph_size.width,
        .height = glyphs_size.height * self.glyph_size.height,
    };
}

/// Returns the `Size` an image will have when converted to `GlyphsRef`.
pub fn sizeAsGlyphs(self: *const Typesetter, rgb_size: Size) Size {
    return Size{
        .width = rgb_size.width / self.glyph_size.width,
        .height = rgb_size.height / self.glyph_size.height,
    };
}

/// Some `Typesetter`s have GlyphsRef whose "pixels" are not squares. The `Size`
/// returned from `sizeAdGlyphs` does not account for that fact. This function
/// can be used to correct the `Size`.
///
/// `ratio` is the aspect ratio for glyphs/characters in the terminal.
///
/// Example: The `wart.typesetters.quadrant` `Typesetter` will convert four pixels of
/// an image (in two rows of two) into one `Glyph` which will be rendered as a
/// single character in the terminal. Since in most terminal fonts the
/// characters are not squares (but rather higher than they are wide), this will
/// elongate the pixels in the vertical direction.
pub fn correct(self: *const Typesetter, size: Size, ratio: f32) Size {
    const factor = @intToFloat(f32, self.glyph_size.width) / (@intToFloat(f32, self.glyph_size.height) * ratio);
    return Size{
        .width = @floatToInt(usize, @intToFloat(f32, size.width) * factor),
        .height = size.height,
    };
}

pub fn workloads(
    self: *const Typesetter,
    glyphs: GlyphsRef,
    image: ImgRefConst,
    jobs: usize,
) WorkloadIterator {
    return WorkloadIterator.init(self, glyphs, image, jobs);
}

/// A `Workload` can be given to a `Typesetter` to convert an `image` to `glyphs`.
/// `Workload`s can be created via a `WorkloadIterator`.
pub const Workload = struct {
    glyphs: GlyphsRef,
    image: ImgRefConst,
};

/// An iterator over all the `Workload`s needed to convert an `image` to
/// `glyphs`.
pub const WorkloadIterator = struct {
    glyphs: GlyphsRef,
    image: ImgRefConst,

    num_more: usize,
    per_thread: usize,
    px_per_row: usize,

    jobs: usize,
    index: usize,

    pub fn init(
        typesetter: *const Typesetter,
        glyphs: GlyphsRef,
        image: ImgRefConst,
        jobs: usize,
    ) WorkloadIterator {
        const in_size = typesetter.sizeAsGlyphs(image.size());

        const len = std.math.min(in_size.height, glyphs.size().height);

        return WorkloadIterator{
            .glyphs = glyphs,
            .image = image,

            .num_more = len % jobs,
            .per_thread = len / jobs,
            .px_per_row = typesetter.glyph_size.height,

            .jobs = jobs,
            .index = 0,
        };
    }

    pub fn next(self: *WorkloadIterator) !?Workload {
        if (self.index >= self.jobs) {
            return null;
        }

        const row_glyphs = if (self.index < self.num_more) self.per_thread + 1 else self.per_thread;
        const row_image = row_glyphs * self.px_per_row;

        const splitGlyphs = try self.glyphs.split(row_glyphs);
        const splitImage = try self.image.splitConst(row_image);

        const workload = Workload{
            .glyphs = splitGlyphs[0],
            .image = splitImage[0],
        };

        self.glyphs = splitGlyphs[1];
        self.image = splitImage[1];

        self.index += 1;

        return workload;
    }
};
