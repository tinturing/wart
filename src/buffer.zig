const std = @import("std");
const Size = matrix.Size;
const Glyph = @import("glyph.zig").Glyph;
const matrix = @import("matrix.zig");
const color = @import("color.zig");

const buffer = @This();

/// A heap allocated, two-dimensional slice of `Glyph`s.
pub const GlyphsRefConst = matrix.MatrixRefConst(Glyph);
pub const GlyphsRef = matrix.MatrixRef(Glyph);
pub const Glyphs = matrix.Matrix(Glyph);

/// A buffer that can be used to arrange `Glyph`s and render them on the
/// terminal using ANSI escape codes.
pub const Buffer = struct {
    managed: Glyphs,
    chars: std.ArrayList(u8),

    const Self = @This();

    pub fn init(allocator: std.mem.Allocator, size: Size) !Self {
        return Self{
            .managed = try Glyphs.init(allocator, size),
            .chars = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn deinit(self: *const Self) void {
        self.managed.deinit();
        self.chars.deinit();
    }

    pub fn glyphs(self: *Self) GlyphsRef {
        return self.managed.ref();
    }

    pub fn set(self: *Self, col: usize, row: usize, glyph: Glyph) !void {
        try self.managed.set(col, row, glyph);
    }

    pub fn get(self: *Self, col: usize, row: usize) !Glyph {
        return try self.managed.get(col, row);
    }

    pub fn preallocate(self: *Self) !void {
        try ensureSize(&self.chars, self.managed.size());
    }

    pub fn render(self: *Self) ![]const u8 {
        try self.chars.resize(0);
        try buffer.render(self.managed.refConst(), self.chars.writer());

        return self.chars.items;
    }
};

/// A buffer that can be used to arrange `Glyph`s and render them on the
/// terminal using ANSI escape codes.
///
/// Unlike `Buffer`, `DoubleBuffer` does not render all `Glyph`s every time
/// `DoubleBuffer.render` is called. Instead, only the first call renders all
/// `Glyph`s, but consecutive calls create an efficient diff between the
/// `current` frame and the `previous` frame that can be used to "patch" the
/// contents of the terminal, by printing it on top of the previous location.
/// This is achieved by using ANSI escape codes to move the terminal cursor.
///
/// This should be treated as a write-only buffer, because `current` and
/// `previous` are swapped after every call to `render`. Therefore, after
/// rendering, `current` does not contain the content that was just rendered,
/// rather the content from two frames ago.
pub const DoubleBuffer = struct {
    previous: Glyphs,
    current: Glyphs,
    chars: std.ArrayList(u8),

    const Self = @This();

    pub fn init(allocator: std.mem.Allocator, size: Size) !Self {
        return Self{
            .previous = try Glyphs.init(allocator, size),
            .current = try Glyphs.init(allocator, size),
            .chars = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn deinit(self: *const Self) void {
        self.previous.deinit();
        self.current.deinit();
        self.chars.deinit();
    }

    pub fn glyphs(self: *Self) GlyphsRef {
        return self.current.ref();
    }

    pub fn set(self: *Self, col: usize, row: usize, glyph: Glyph) !void {
        try self.current.set(col, row, glyph);
    }

    pub fn preallocate(self: *Self) !void {
        std.debug.assert(std.meta.eql(self.previous.size(), self.current.size()));

        try ensureSize(&self.chars, self.previous.size());
    }

    pub fn render(self: *Self) ![]const u8 {
        std.debug.assert(std.meta.eql(self.previous.size(), self.current.size()));

        try self.chars.resize(0);
        try renderDiff(self.previous.refConst(), self.current.refConst(), self.chars.writer());

        std.mem.swap(Glyphs, &self.previous, &self.current);

        return self.chars.items;
    }
};

const numbers = [256][]const u8{
    "0",   "1",   "2",   "3",   "4",   "5",   "6",   "7",   "8",   "9",   "10",  "11",  "12",  "13",  "14",  "15",
    "16",  "17",  "18",  "19",  "20",  "21",  "22",  "23",  "24",  "25",  "26",  "27",  "28",  "29",  "30",  "31",
    "32",  "33",  "34",  "35",  "36",  "37",  "38",  "39",  "40",  "41",  "42",  "43",  "44",  "45",  "46",  "47",
    "48",  "49",  "50",  "51",  "52",  "53",  "54",  "55",  "56",  "57",  "58",  "59",  "60",  "61",  "62",  "63",
    "64",  "65",  "66",  "67",  "68",  "69",  "70",  "71",  "72",  "73",  "74",  "75",  "76",  "77",  "78",  "79",
    "80",  "81",  "82",  "83",  "84",  "85",  "86",  "87",  "88",  "89",  "90",  "91",  "92",  "93",  "94",  "95",
    "96",  "97",  "98",  "99",  "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111",
    "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127",
    "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143",
    "144", "145", "146", "147", "148", "149", "150", "151", "152", "153", "154", "155", "156", "157", "158", "159",
    "160", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170", "171", "172", "173", "174", "175",
    "176", "177", "178", "179", "180", "181", "182", "183", "184", "185", "186", "187", "188", "189", "190", "191",
    "192", "193", "194", "195", "196", "197", "198", "199", "200", "201", "202", "203", "204", "205", "206", "207",
    "208", "209", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220", "221", "222", "223",
    "224", "225", "226", "227", "228", "229", "230", "231", "232", "233", "234", "235", "236", "237", "238", "239",
    "240", "241", "242", "243", "244", "245", "246", "247", "248", "249", "250", "251", "252", "253", "254", "255",
};

fn ensureSize(chars: *std.ArrayList(u8), size: Size) !void {
    // 19 byte foreground: \x1b[38;2;000;000;000m
    // 19 byte background: \x1b[48;2;000;000;000m
    // 4 byte codepoint
    const bytes_per_glyph = 42;

    // 4 byte reset: \x1b[0m
    // 1 byte newline: \n
    const bytes_per_line_ending = 5;

    const required_size = (bytes_per_glyph * size.width + bytes_per_line_ending) * size.height;

    try chars.ensureTotalCapacity(required_size);
}

const reset = "\x1b[0m";

fn renderAnsiColor24(
    comptime n: []const u8,
    rgb: color.Rgb,
    writer: anytype,
) !void {
    try writer.writeAll("\x1b[" ++ n ++ ";");
    try writer.writeAll(numbers[rgb.r]);
    try writer.writeAll(";");
    try writer.writeAll(numbers[rgb.g]);
    try writer.writeAll(";");
    try writer.writeAll(numbers[rgb.b]);
    try writer.writeAll("m");
}

fn renderForeground(fg: color.Rgb, writer: anytype) !void {
    try renderAnsiColor24("38;2", fg, writer);
}

fn renderBackground(bg: color.Rgb, writer: anytype) !void {
    try renderAnsiColor24("48;2", bg, writer);
}

fn renderCodepoint(codepoint: u21, writer: anytype) !void {
    var buf: [4]u8 = undefined;
    const len = try std.unicode.utf8Encode(codepoint, &buf);
    try writer.writeAll(buf[0..len]);
}

pub fn render(glyphs: GlyphsRefConst, writer: anytype) !void {
    var fg_last: ?color.Rgb = null;
    var bg_last: ?color.Rgb = null;

    var rows = glyphs.rowsConst();

    while (rows.next()) |row| {
        for (row) |*glyph| {
            if (glyph.foreground) |fg| {
                if (!std.meta.eql(fg_last, fg)) {
                    try renderForeground(fg, writer);
                    fg_last = fg;
                }
            }
            if (glyph.background) |bg| {
                if (!std.meta.eql(bg_last, bg)) {
                    try renderBackground(bg, writer);
                    bg_last = bg;
                }
            }
            try renderCodepoint(glyph.codepoint, writer);
        }

        try writer.writeAll(reset);
        try writer.writeAll("\r\n");

        fg_last = null;
        bg_last = null;
    }
}

const Cursor = struct {
    row: usize = 0,
    col: usize = 0,
};

pub fn renderDiff(prev: GlyphsRefConst, next: GlyphsRefConst, writer: anytype) !void {
    if (!std.meta.eql(prev.size(), next.size())) {
        return error.DifferentSize;
    }

    const width = next.size().width;
    const height = next.size().height;

    var fg_last: ?color.Rgb = null;
    var bg_last: ?color.Rgb = null;

    var cursor = Cursor{};
    var terminal_cursor = Cursor{};

    var prev_slice = prev.items();
    var next_slice = next.items();

    while (cursor.row < height) : ({
        cursor.row += 1;
        cursor.col = 0;
    }) {
        while (cursor.col < width) : ({
            cursor.col += 1;
            prev_slice = prev_slice[1..];
            next_slice = next_slice[1..];
        }) {
            const prev_glyph = &prev_slice[0];
            const glyph = &next_slice[0];

            if (!std.meta.eql(prev_glyph.*, glyph.*)) {
                if (cursor.row > terminal_cursor.row) {
                    try writer.print("\x1b[{d}B", .{cursor.row - terminal_cursor.row});
                }

                if (cursor.col > terminal_cursor.col) {
                    try writer.print("\x1b[{d}C", .{cursor.col - terminal_cursor.col});
                } else if (cursor.col < terminal_cursor.col) {
                    try writer.print("\x1b[{d}D", .{terminal_cursor.col - cursor.col});
                }

                if (glyph.foreground) |fg| {
                    if (!std.meta.eql(fg_last, fg)) {
                        try renderForeground(fg, writer);
                        fg_last = fg;
                    }
                }
                if (glyph.background) |bg| {
                    if (!std.meta.eql(bg_last, bg)) {
                        try renderBackground(bg, writer);
                        bg_last = bg;
                    }
                }
                try renderCodepoint(glyph.codepoint, writer);

                terminal_cursor = cursor;
                terminal_cursor.col += 1;
            }
        }

        if (std.meta.eql(cursor, terminal_cursor)) {
            try writer.writeAll(reset);
            if (cursor.row + 1 < height) {
                try writer.print("\x1b[{d}D\x1b[B", .{width});

                terminal_cursor.col = 0;
                terminal_cursor.row += 1;
            }

            fg_last = null;
            bg_last = null;
        }
    }

    if (cursor.row > terminal_cursor.row + 1) {
        try writer.print("\x1b[{d}B", .{cursor.row - terminal_cursor.row - 1});
    }

    if (width > terminal_cursor.col) {
        try writer.print("\x1b[{d}C", .{width - terminal_cursor.col});
    }
}
