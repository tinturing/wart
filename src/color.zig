/// A 24-bit RGB color.
///
/// A byte slice `[]u8` of 24-bit pixels (e.g. from a P6 Netpbm) can be
/// converted to a `[]Rgb`:
///
///     const bytes = [_]u8{ 255, 0, 0 };
///     std.mem.bytesAsSlice(Rgb, bytes[0..]);
pub const Rgb = extern struct {
    r: u8,
    g: u8,
    b: u8,

    pub fn init(r: u8, g: u8, b: u8) Rgb {
        return Rgb{
            .r = r,
            .g = g,
            .b = b,
        };
    }

    pub fn luminosity(self: Rgb) u8 {
        return @floatToInt(u8, (0.2126 * @intToFloat(f64, self.r)) +
            (0.7152 * @intToFloat(f64, self.g)) +
            (0.0722 * @intToFloat(f64, self.b)));
    }
};

/// A struct to calculate the average `Rgb` color of a number of pixels.
pub const Average = struct {
    r_sum: u64 = 0,
    g_sum: u64 = 0,
    b_sum: u64 = 0,
    count: usize = 0,

    pub fn add(self: *Average, rgb: Rgb) void {
        self.r_sum += rgb.r;
        self.g_sum += rgb.g;
        self.b_sum += rgb.b;
        self.count += 1;
    }

    pub fn get(self: Average) ?Rgb {
        if (self.count != 0) {
            const r = @intCast(u8, self.r_sum / self.count);
            const g = @intCast(u8, self.g_sum / self.count);
            const b = @intCast(u8, self.b_sum / self.count);
            return Rgb.init(r, g, b);
        } else {
            return null;
        }
    }
};
