const testing = @import("std").testing;

const Chunks = @import("wart").Chunks;
const ChunksConst = @import("wart").chunks.ChunksConst;

test "Chunks.next" {
    var values = [10]u8{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    var chunks = Chunks(u8).init(values[0..], 3);

    try testing.expectEqualSlices(u8, &.{ 1, 2, 3 }, chunks.next().?);

    const chunk = chunks.next().?;
    try testing.expectEqualSlices(u8, &.{ 4, 5, 6 }, chunk);
    chunk[1] += 37;

    try testing.expectEqualSlices(u8, &.{ 7, 8, 9 }, chunks.next().?);
    try testing.expectEqual(@as(?[]u8, null), chunks.next());

    // NOTE the remaining `10` is unreachable.

    try testing.expectEqualSlices(u8, &.{ 1, 2, 3, 4, 42, 6, 7, 8, 9, 10 }, values[0..]);
}

test "ChunksConst.next" {
    var values = [10]u8{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    var chunks = ChunksConst(u8).init(values[0..], 3);

    try testing.expectEqualSlices(u8, &.{ 1, 2, 3 }, chunks.next().?);
    try testing.expectEqualSlices(u8, &.{ 4, 5, 6 }, chunks.next().?);
    try testing.expectEqualSlices(u8, &.{ 7, 8, 9 }, chunks.next().?);
    try testing.expectEqual(@as(?[]const u8, null), chunks.next());

    // NOTE the remaining `10` is unreachable.
}
