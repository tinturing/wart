const std = @import("std");
const testing = std.testing;

const wart = @import("wart");
const color = wart.color;
const image = wart.image;
const Glyphs = wart.buffer.Glyphs;

const black = wart.color.Rgb.init(0, 0, 0);
const white = wart.color.Rgb.init(255, 255, 255);
const red = wart.color.Rgb.init(255, 0, 0);
const green = wart.color.Rgb.init(0, 255, 0);
const blue = wart.color.Rgb.init(0, 0, 255);
const cyan = wart.color.Rgb.init(0, 255, 255);
const magenta = wart.color.Rgb.init(255, 0, 255);
const yellow = wart.color.Rgb.init(255, 255, 0);

test "typesetters.asymmetric compose" {
    var rgb = [_]wart.color.Rgb{
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
        black, black, black, white,
    };

    const asymmetric = wart.typesetters.asymmetric();
    const size = asymmetric.sizeAsGlyphs(.{ .width = 4, .height = 8 });

    try testing.expectEqual(size, .{ .width = 1, .height = 1 });

    var managed = try Glyphs.init(std.testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 4, .height = 8 },
    );
    try asymmetric.compose(glyphs, img);

    const actual = glyphs.items();

    const expected = wart.Glyph{
        .codepoint = '▊',
        .foreground = black,
        .background = white,
    };

    try testing.expectEqualSlices(wart.Glyph, &.{expected}, actual);
}

test "typesetters.block compose" {
    var rgb = [_]wart.color.Rgb{
        white,
    };

    const block = wart.typesetters.block();
    const size = block.sizeAsGlyphs(.{ .width = 1, .height = 1 });

    try testing.expectEqual(size, .{ .width = 1, .height = 1 });

    var managed = try Glyphs.init(std.testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 1, .height = 1 },
    );
    try block.compose(glyphs, img);

    const actual = glyphs.items();

    const expected = wart.Glyph{
        .codepoint = ' ',
        .foreground = null,
        .background = white,
    };

    try testing.expectEqualSlices(wart.Glyph, &.{expected}, actual);
}

test "typesetters.half compose" {
    var rgb = [_]wart.color.Rgb{
        blue,
        yellow,
    };

    const half = wart.typesetters.half();
    const size = half.sizeAsGlyphs(.{ .width = 1, .height = 2 });

    try testing.expectEqual(size, .{ .width = 1, .height = 1 });

    var managed = try Glyphs.init(std.testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 1, .height = 2 },
    );
    try half.compose(glyphs, img);

    const actual = glyphs.items();

    const expected = wart.Glyph{
        .codepoint = '▄',
        .foreground = yellow,
        .background = blue,
    };

    try testing.expectEqualSlices(wart.Glyph, &.{expected}, actual);
}

test "typesetters.quadrant compose" {
    var rgb = [_]wart.color.Rgb{
        red,   white,
        white, white,
    };

    const quadrant = wart.typesetters.quadrant();
    const size = quadrant.sizeAsGlyphs(.{ .width = 2, .height = 2 });

    try testing.expectEqual(size, .{ .width = 1, .height = 1 });

    var managed = try Glyphs.init(std.testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 2, .height = 2 },
    );
    try quadrant.compose(glyphs, img);

    const actual = glyphs.items();

    const expected = wart.Glyph{
        .codepoint = '▟',
        .foreground = white,
        .background = red,
    };

    try testing.expectEqualSlices(wart.Glyph, &.{expected}, actual);
}

test "typesetters.sextant compose" {
    var rgb = [_]wart.color.Rgb{
        black, magenta,
        black, magenta,
        black, magenta,
    };

    const sextant = wart.typesetters.sextant();
    const size = sextant.sizeAsGlyphs(.{ .width = 2, .height = 3 });

    try testing.expectEqual(size, .{ .width = 1, .height = 1 });

    var managed = try Glyphs.init(std.testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 2, .height = 3 },
    );
    try sextant.compose(glyphs, img);

    const actual = glyphs.items();

    const expected = wart.Glyph{
        .codepoint = '▐',
        .foreground = magenta,
        .background = black,
    };

    try testing.expectEqualSlices(wart.Glyph, &.{expected}, actual);
}

test "typesetters.half composeParallel" {
    var rgb = [_]wart.color.Rgb{
        green, red,
        green, yellow,
        cyan,  blue,
        black, blue,
    };

    const half = wart.typesetters.half();
    const size = half.sizeAsGlyphs(.{ .width = 2, .height = 4 });

    try testing.expectEqual(size, .{ .width = 2, .height = 2 });

    var managed = try Glyphs.init(testing.allocator, size);
    defer managed.deinit();
    var glyphs = managed.ref();

    const img = try image.ImgRefConst.init(
        rgb[0..],
        .{ .width = 2, .height = 4 },
    );
    // NOTE More jobs are used than are needed,
    // more jobs than will actually do any work.
    try half.composeParallel(testing.allocator, glyphs, img, 8);

    const actual = glyphs.items();

    const expected = [4]wart.Glyph{
        .{
            .codepoint = ' ',
            .foreground = null,
            .background = green,
        },
        .{
            .codepoint = '▄',
            .foreground = yellow,
            .background = red,
        },
        .{
            .codepoint = '▀',
            .foreground = cyan,
            .background = black,
        },
        .{
            .codepoint = ' ',
            .foreground = null,
            .background = blue,
        },
    };

    try testing.expectEqualSlices(wart.Glyph, expected[0..], actual);
}
