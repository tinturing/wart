const std = @import("std");
const testing = std.testing;

const wart = @import("wart");
const matrix = wart.matrix;
const Size = wart.Size;

test "matrix.MatrixRefConst.init" {
    const values = [_]u8{
        0, 1, 2,
        3, 4, 5,
    };

    const ref = try matrix.MatrixRefConst(u8).init(values[0..], .{ .width = 3, .height = 2 });

    try testing.expectEqual(@as([]const u8, values[0..]).ptr, ref.items().ptr);
    try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 4, 5 }, ref.items());
    try testing.expectEqual(Size{ .width = 3, .height = 2 }, ref.size());

    const err = matrix.MatrixRefConst(u8).init(values[1..], .{ .width = 3, .height = 2 });
    try testing.expectError(matrix.Error.InvalidSize, err);
}

test "matrix.MatrixRef.init" {
    var values = [_]u8{
        0, 1, 2,
        3, 4, 5,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 3, .height = 2 });

    try testing.expectEqual(@as([]u8, values[0..]).ptr, ref.items().ptr);
    try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 4, 5 }, ref.items());
    try testing.expectEqual(Size{ .width = 3, .height = 2 }, ref.size());

    const err = matrix.MatrixRef(u8).init(values[1..], .{ .width = 3, .height = 2 });
    try testing.expectError(matrix.Error.InvalidSize, err);
}

test "matrix.Matrix.init" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 3, .height = 2 });
    defer mat.deinit();

    try testing.expectEqual(@as(usize, 6), mat.items().len);
    try testing.expectEqual(Size{ .width = 3, .height = 2 }, mat.size());

    const err = matrix.Matrix(u8).init(testing.failing_allocator, .{ .width = 3, .height = 2 });
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.MatrixRef.ref" {
    var values = [_]f32{
        0.1, 1.2,
        3.4, 5.6,
    };

    const ref = try matrix.MatrixRef(f32).init(values[0..], .{ .width = 2, .height = 2 });

    const refref = ref.ref();

    try testing.expectEqual(ref, refref);
}

test "matrix.Matrix.ref" {
    const mat = try matrix.Matrix(f32).init(std.testing.allocator, .{ .width = 2, .height = 2 });
    defer mat.deinit();

    const ref = mat.ref();

    try testing.expectEqual(mat.items().ptr, ref.items().ptr);
    try testing.expectEqual(mat.size(), ref.size());
}

test "matrix.MatrixRefConst.refConst" {
    const values = [_]f32{
        0.1, 1.2,
        3.4, 5.6,
    };

    const ref = try matrix.MatrixRefConst(f32).init(values[0..], .{ .width = 2, .height = 2 });

    const refref = ref.refConst();

    try testing.expectEqual(ref, refref);
}

test "matrix.MatrixRef.refConst" {
    var values = [_]f32{
        0.1, 1.2,
        3.4, 5.6,
    };

    const ref = try matrix.MatrixRef(f32).init(values[0..], .{ .width = 2, .height = 2 });

    const refref = ref.refConst();

    try testing.expectEqual(@as([]const f32, ref.items()).ptr, refref.items().ptr);
    try testing.expectEqual(ref.size(), refref.size());
}

test "matrix.Matrix.refConst" {
    const mat = try matrix.Matrix(f32).init(std.testing.allocator, .{ .width = 2, .height = 2 });
    defer mat.deinit();

    const ref = mat.refConst();

    try testing.expectEqual(@as([]const f32, mat.items()).ptr, ref.items().ptr);
    try testing.expectEqual(mat.size(), ref.size());
}

test "matrix.MatrixRefConst.promote" {
    const values = [_]i32{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRefConst(i32).init(values[0..], .{ .width = 3, .height = 3 });

    const mat = try ref.promote(std.testing.allocator);
    defer mat.deinit();

    try testing.expect(mat.items().ptr != ref.items().ptr);
    try testing.expectEqualSlices(i32, ref.items(), mat.items());
    try testing.expectEqual(ref.size(), mat.size());

    const err = ref.promote(testing.failing_allocator);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.MatrixRef.promote" {
    var values = [_]i32{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRef(i32).init(values[0..], .{ .width = 3, .height = 3 });

    const mat = try ref.promote(std.testing.allocator);
    defer mat.deinit();

    try testing.expect(mat.items().ptr != ref.items().ptr);
    try testing.expectEqualSlices(i32, ref.items(), mat.items());
    try testing.expectEqual(ref.size(), mat.size());

    const err = ref.promote(testing.failing_allocator);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.Matrix.promote" {
    const mat = try matrix.Matrix(i32).init(std.testing.allocator, .{ .width = 3, .height = 3 });
    defer mat.deinit();

    const promoted = try mat.promote(std.testing.allocator);
    defer promoted.deinit();

    try testing.expect(promoted.items().ptr != mat.items().ptr);
    try testing.expectEqualSlices(i32, promoted.items(), mat.items());
    try testing.expectEqual(promoted.size(), mat.size());

    const err = mat.promote(testing.failing_allocator);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.MatrixRefConst.get" {
    const values = [_]u8{
        0, 1, 2,
        3, 4, 5,
    };

    const ref = try matrix.MatrixRefConst(u8).init(values[0..], .{ .width = 3, .height = 2 });

    const actual = try ref.get(2, 1);
    try testing.expectEqual(@as(u8, 5), actual);

    const err = ref.get(0, 2);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.MatrixRef.get" {
    var values = [_]u8{
        0, 1, 2,
        3, 4, 5,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 3, .height = 2 });

    const actual = try ref.get(2, 1);
    try testing.expectEqual(@as(u8, 5), actual);

    const err = ref.get(0, 2);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.Matrix.get" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 3, .height = 2 });
    defer mat.deinit();

    const actual = try mat.get(2, 1);
    try testing.expectEqual(@as(u8, mat.items()[5]), actual);

    const err = mat.get(0, 2);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.MatrixRef.getPtr" {
    var values = [_]u8{
        0, 1,
        2, 3,
        4, 5,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 2, .height = 3 });

    const ptr = try ref.getPtr(0, 2);
    ptr.* = 42;

    try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 42, 5 }, values[0..]);

    const err = ref.getPtr(0, 3);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.Matrix.getPtr" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 2, .height = 3 });
    defer mat.deinit();

    const ptr = try mat.getPtr(0, 2);
    ptr.* = 42;

    try testing.expectEqual(@as(*u8, &mat.items()[4]), ptr);
    try testing.expectEqual(@as(u8, 42), mat.items()[4]);

    const err = mat.getPtr(0, 3);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.MatrixRefConst.getPtrConst" {
    const values = [_]u8{
        0, 1,
        2, 3,
        4, 5,
    };

    const ref = try matrix.MatrixRefConst(u8).init(values[0..], .{ .width = 2, .height = 3 });

    const ptr = try ref.getPtrConst(0, 2);

    try testing.expectEqual(@as(u8, 4), ptr.*);

    const err = ref.getPtrConst(0, 3);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.MatrixRef.getPtrConst" {
    var values = [_]u8{
        0, 1,
        2, 3,
        4, 5,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 2, .height = 3 });

    const ptr = try ref.getPtrConst(0, 2);

    try testing.expectEqual(@as(u8, 4), ptr.*);

    const err = ref.getPtrConst(0, 3);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.Matrix.getPtrConst" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 2, .height = 3 });
    defer mat.deinit();

    const ptr = try mat.getPtrConst(0, 2);

    try testing.expectEqual(@as(*const u8, &mat.items()[4]), ptr);

    const err = mat.getPtrConst(0, 3);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

fn square(x: i32) i64 {
    return x * x;
}

test "matrix.MatrixRefConst.map" {
    const values = [_]i32{
        0, 1,
        2, 3,
    };

    const ref = try matrix.MatrixRefConst(i32).init(values[0..], .{ .width = 2, .height = 2 });

    const mat = try ref.map(i64, std.testing.allocator, square);
    defer mat.deinit();

    try testing.expectEqual(Size{ .width = 2, .height = 2 }, mat.size());
    try testing.expectEqualSlices(i64, &.{ 0, 1, 4, 9 }, mat.items());

    const err = ref.map(i64, testing.failing_allocator, square);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.MatrixRef.map" {
    var values = [_]i32{
        0, 1,
        2, 3,
    };

    const ref = try matrix.MatrixRef(i32).init(values[0..], .{ .width = 2, .height = 2 });

    const mat = try ref.map(i64, std.testing.allocator, square);
    defer mat.deinit();

    try testing.expectEqual(Size{ .width = 2, .height = 2 }, mat.size());
    try testing.expectEqualSlices(i64, &.{ 0, 1, 4, 9 }, mat.items());

    const err = ref.map(i64, testing.failing_allocator, square);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.Matrix.map" {
    const mat = try matrix.Matrix(i32).init(std.testing.allocator, .{ .width = 2, .height = 2 });
    defer mat.deinit();

    std.mem.copy(i32, mat.items(), &.{ 0, 1, 2, 3 });

    const mapped = try mat.map(i64, std.testing.allocator, square);
    defer mapped.deinit();

    try testing.expectEqual(Size{ .width = 2, .height = 2 }, mapped.size());
    try testing.expectEqualSlices(i64, &.{ 0, 1, 4, 9 }, mapped.items());

    const err = mat.map(i64, testing.failing_allocator, square);
    try testing.expectError(std.mem.Allocator.Error.OutOfMemory, err);
}

test "matrix.MatrixRef.set" {
    var values = [_]u8{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 3, .height = 3 });

    try ref.set(0, 0, 42);

    try testing.expectEqualSlices(u8, &.{ 42, 1, 2, 3, 4, 5, 6, 7, 8 }, values[0..]);

    const err = ref.set(0, 3, 99);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.Matrix.set" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 3, .height = 3 });
    defer mat.deinit();

    try mat.set(0, 0, 42);
    try mat.set(1, 0, 99);

    try testing.expectEqualSlices(u8, &.{ 42, 99 }, mat.items()[0..2]);

    const err = mat.set(0, 3, 99);
    try testing.expectError(matrix.Error.OutOfBounds, err);
}

test "matrix.MatrixRef.rows" {
    var values = [_]u8{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 3, .height = 3 });
    var rows = ref.rows();

    try testing.expectEqualSlices(u8, &.{ 0, 1, 2 }, rows.next().?);

    const slice = rows.next().?;
    try testing.expectEqualSlices(u8, &.{ 3, 4, 5 }, slice);
    slice[1] = 42;

    try testing.expectEqualSlices(u8, &.{ 6, 7, 8 }, rows.next().?);
    try testing.expectEqual(@as(?[]u8, null), rows.next());

    try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 42, 5, 6, 7, 8 }, values[0..]);
}

test "matrix.Matrix.rows" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 3, .height = 3 });
    defer mat.deinit();

    var rows = mat.rows();

    try testing.expectEqualSlices(u8, mat.items()[0..3], rows.next().?);

    const slice = rows.next().?;
    try testing.expectEqualSlices(u8, mat.items()[3..6], slice);
    slice[1] = 42;

    try testing.expectEqualSlices(u8, mat.items()[6..], rows.next().?);
    try testing.expectEqual(@as(?[]u8, null), rows.next());

    try testing.expectEqual(@as(u8, 42), try mat.get(1, 1));
}

test "matrix.MatrixRefConst.rowsConst" {
    const values = [_]u8{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRefConst(u8).init(values[0..], .{ .width = 3, .height = 3 });

    var rows = ref.rowsConst();

    try testing.expectEqualSlices(u8, &.{ 0, 1, 2 }, rows.next().?);
    try testing.expectEqualSlices(u8, &.{ 3, 4, 5 }, rows.next().?);
    try testing.expectEqualSlices(u8, &.{ 6, 7, 8 }, rows.next().?);
    try testing.expectEqual(@as(?[]const u8, null), rows.next());
}

test "matrix.MatrixRef.rowsConst" {
    var values = [_]u8{
        0, 1, 2,
        3, 4, 5,
        6, 7, 8,
    };

    const ref = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 3, .height = 3 });

    var rows = ref.rowsConst();

    try testing.expectEqualSlices(u8, &.{ 0, 1, 2 }, rows.next().?);
    try testing.expectEqualSlices(u8, &.{ 3, 4, 5 }, rows.next().?);
    try testing.expectEqualSlices(u8, &.{ 6, 7, 8 }, rows.next().?);
    try testing.expectEqual(@as(?[]const u8, null), rows.next());
}

test "matrix.Matrix.rowsConst" {
    const mat = try matrix.Matrix(u8).init(testing.allocator, .{ .width = 3, .height = 3 });
    defer mat.deinit();

    var rows = mat.rowsConst();

    try testing.expectEqualSlices(u8, mat.items()[0..3], rows.next().?);
    try testing.expectEqualSlices(u8, mat.items()[3..6], rows.next().?);
    try testing.expectEqualSlices(u8, mat.items()[6..], rows.next().?);
    try testing.expectEqual(@as(?[]const u8, null), rows.next());
}

test "matrix.MatrixRef.split" {
    var values = [_]u8{
        0, 1,
        2, 3,
        4, 5,
        6, 7,
        8, 9,
    };

    const src = try matrix.MatrixRef(u8).init(values[0..], .{ .width = 2, .height = 5 });

    {
        const splits = try src.split(3);

        try testing.expectEqual(Size{ .width = 2, .height = 3 }, splits[0].size());
        try testing.expectEqual(Size{ .width = 2, .height = 2 }, splits[1].size());

        try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 4, 5 }, splits[0].items());
        try testing.expectEqualSlices(u8, &.{ 6, 7, 8, 9 }, splits[1].items());
    }

    {
        const splits = try src.split(0);

        try testing.expectEqual(Size{ .width = 2, .height = 0 }, splits[0].size());
        try testing.expectEqualSlices(u8, &.{}, splits[0].items());

        try testing.expectEqual(src, splits[1]);
    }

    {
        const splits = try src.split(5);

        try testing.expectEqual(src, splits[0]);

        try testing.expectEqual(Size{ .width = 2, .height = 0 }, splits[1].size());
        try testing.expectEqualSlices(u8, &.{}, splits[1].items());
    }

    {
        var no_values = [0]u8{};

        const empty = try matrix.MatrixRef(u8).init(no_values[0..], .{ .width = 0, .height = 0 });

        const splits = try empty.split(0);

        try testing.expectEqual(empty, splits[0]);
        try testing.expectEqual(empty, splits[1]);
    }

    {
        const err = src.split(6);
        try testing.expectError(matrix.Error.OutOfBounds, err);
    }
}

test "matrix.MatrixRefConst.splitConst" {
    const values = [_]u8{
        0, 1,
        2, 3,
        4, 5,
        6, 7,
        8, 9,
    };

    const src = try matrix.MatrixRefConst(u8).init(values[0..], .{ .width = 2, .height = 5 });

    {
        const splits = try src.splitConst(3);

        try testing.expectEqual(Size{ .width = 2, .height = 3 }, splits[0].size());
        try testing.expectEqual(Size{ .width = 2, .height = 2 }, splits[1].size());

        try testing.expectEqualSlices(u8, &.{ 0, 1, 2, 3, 4, 5 }, splits[0].items());
        try testing.expectEqualSlices(u8, &.{ 6, 7, 8, 9 }, splits[1].items());
    }

    {
        const splits = try src.splitConst(0);

        try testing.expectEqual(Size{ .width = 2, .height = 0 }, splits[0].size());
        try testing.expectEqualSlices(u8, &.{}, splits[0].items());

        try testing.expectEqual(src, splits[1]);
    }

    {
        const splits = try src.splitConst(5);

        try testing.expectEqual(src, splits[0]);

        try testing.expectEqual(Size{ .width = 2, .height = 0 }, splits[1].size());
        try testing.expectEqualSlices(u8, &.{}, splits[1].items());
    }

    {
        var no_values = [0]u8{};

        const empty = try matrix.MatrixRefConst(u8).init(no_values[0..], .{ .width = 0, .height = 0 });

        const splits = try empty.splitConst(0);

        try testing.expectEqual(empty, splits[0]);
        try testing.expectEqual(empty, splits[1]);
    }

    {
        const err = src.splitConst(6);
        try testing.expectError(matrix.Error.OutOfBounds, err);
    }
}
