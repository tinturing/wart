const std = @import("std");

const wart = @import("wart");

const black = wart.color.Rgb.init(0, 0, 0);
const white = wart.color.Rgb.init(255, 255, 255);
const red = wart.color.Rgb.init(255, 0, 0);
const green = wart.color.Rgb.init(0, 255, 0);
const blue = wart.color.Rgb.init(0, 0, 255);
const cyan = wart.color.Rgb.init(0, 255, 255);
const magenta = wart.color.Rgb.init(255, 0, 255);
const yellow = wart.color.Rgb.init(255, 255, 0);

test "Buffer.render" {
    var buffer = try wart.Buffer.init(std.testing.allocator, .{ .height = 2, .width = 2 });
    defer buffer.deinit();

    try buffer.set(0, 0, wart.Glyph.init('A', red, green));
    try buffer.set(1, 0, wart.Glyph.init('B', blue, green));
    try buffer.set(0, 1, wart.Glyph.init('C', blue, green));
    try buffer.set(1, 1, wart.Glyph.init('D', blue, white));

    const actual = try buffer.render();

    const expected: []const u8 =
        "\x1b[38;2;255;0;0m\x1b[48;2;0;255;0mA" ++
        "\x1b[38;2;0;0;255mB" ++
        "\x1b[0m\r\n" ++
        "\x1b[38;2;0;0;255m\x1b[48;2;0;255;0mC" ++
        "\x1b[48;2;255;255;255mD" ++
        "\x1b[0m\r\n";

    try std.testing.expectEqualStrings(expected, actual);
}

test "DoubleBuffer.render" {
    var buffer = try wart.DoubleBuffer.init(std.testing.allocator, .{ .height = 2, .width = 2 });
    defer buffer.deinit();

    try buffer.set(0, 0, wart.Glyph.init('A', red, green));
    try buffer.set(1, 0, wart.Glyph.init('B', blue, green));
    try buffer.set(0, 1, wart.Glyph.init('C', blue, green));
    try buffer.set(1, 1, wart.Glyph.init('D', blue, white));

    const actual0 = try buffer.render();

    const expected0: []const u8 =
        "\x1b[38;2;255;0;0m\x1b[48;2;0;255;0mA" ++
        "\x1b[38;2;0;0;255mB" ++
        "\x1b[0m\x1b[2D\x1b[B" ++
        "\x1b[38;2;0;0;255m\x1b[48;2;0;255;0mC" ++
        "\x1b[48;2;255;255;255mD" ++
        "\x1b[0m";

    try std.testing.expectEqualStrings(expected0, actual0);

    try buffer.set(0, 0, wart.Glyph.init('A', red, green));
    try buffer.set(1, 0, wart.Glyph.init('X', blue, green));
    try buffer.set(0, 1, wart.Glyph.init('C', blue, red));
    try buffer.set(1, 1, wart.Glyph.init('D', blue, white));

    const actual1 = try buffer.render();

    const expected1: []const u8 =
        "\x1b[1C" ++
        "\x1b[38;2;0;0;255m\x1b[48;2;0;255;0mX" ++
        "\x1b[0m\x1b[2D\x1b[B" ++
        "\x1b[38;2;0;0;255m\x1b[48;2;255;0;0mC" ++
        "\x1b[1C";

    try std.testing.expectEqualStrings(expected1, actual1);
}

test "DoubleBuffer.render same content twice" {
    var buffer = try wart.DoubleBuffer.init(std.testing.allocator, .{ .height = 2, .width = 2 });
    defer buffer.deinit();

    try buffer.set(0, 0, wart.Glyph.init('A', red, green));
    try buffer.set(1, 0, wart.Glyph.init('B', blue, green));
    try buffer.set(0, 1, wart.Glyph.init('C', blue, green));
    try buffer.set(1, 1, wart.Glyph.init('D', blue, white));

    const actual0 = try buffer.render();

    const expected0: []const u8 =
        "\x1b[38;2;255;0;0m\x1b[48;2;0;255;0mA" ++
        "\x1b[38;2;0;0;255mB" ++
        "\x1b[0m\x1b[2D\x1b[B" ++
        "\x1b[38;2;0;0;255m\x1b[48;2;0;255;0mC" ++
        "\x1b[48;2;255;255;255mD" ++
        "\x1b[0m";

    try std.testing.expectEqualStrings(expected0, actual0);

    try buffer.set(0, 0, wart.Glyph.init('A', red, green));
    try buffer.set(1, 0, wart.Glyph.init('B', blue, green));
    try buffer.set(0, 1, wart.Glyph.init('C', blue, green));
    try buffer.set(1, 1, wart.Glyph.init('D', blue, white));

    const actual1 = try buffer.render();

    const expected1: []const u8 =
        "\x1b[1B\x1b[2C";

    try std.testing.expectEqualStrings(expected1, actual1);
}

test "Buffer.preallocate" {
    var buffer = try wart.Buffer.init(std.testing.allocator, .{ .height = 10, .width = 10 });
    defer buffer.deinit();

    try std.testing.expectEqual(@as(usize, 0), buffer.chars.capacity);
    try buffer.preallocate();
    // NOTE `preallocate` tries to set the capactity to 4250 (see
    // `buffer.ensureSize`), but `std.ArrayList(u8).ensureTotalCapacity` sets
    // it to the next power of 2. Therefore, it should be 8192.
    try std.testing.expectEqual(@as(usize, 8192), buffer.chars.capacity);
}

test "DoubleBuffer.preallocate" {
    var buffer = try wart.DoubleBuffer.init(std.testing.allocator, .{ .height = 10, .width = 10 });
    defer buffer.deinit();

    try std.testing.expectEqual(@as(usize, 0), buffer.chars.capacity);
    try buffer.preallocate();
    // NOTE `preallocate` tries to set the capactity to 4250 (see
    // `buffer.ensureSize`), but `std.ArrayList(u8).ensureTotalCapacity` sets
    // it to the next power of 2. Therefore, it should be 8192.
    try std.testing.expectEqual(@as(usize, 8192), buffer.chars.capacity);
}
