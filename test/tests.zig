test "wart test suite" {
    _ = @import("typesetters.zig");
    _ = @import("buffer.zig");
    _ = @import("chunks.zig");
    _ = @import("image.zig");
    _ = @import("matrix.zig");
    _ = @import("term.zig");
}
