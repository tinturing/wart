const std = @import("std");

const wart = @import("wart");
const Key = wart.term.Key;

test "Keys.next" {
    var values = [_]u8{
        'a', 'b', 27,
        27,  '[', 'A',
        27,  '[', 'B',
        27,  '[', 'C',
        27,  '[', 'D',
        27,  '[', 'H',
        3,   27,  '[',
    };

    var keys = wart.term.Keys.init(values[0..]);

    try std.testing.expectEqual(Key{ .char = 'a' }, keys.next().?);
    try std.testing.expectEqual(Key{ .char = 'b' }, keys.next().?);
    try std.testing.expectEqual(Key.esc, keys.next().?);
    try std.testing.expectEqual(Key.up, keys.next().?);
    try std.testing.expectEqual(Key.down, keys.next().?);
    try std.testing.expectEqual(Key.right, keys.next().?);
    try std.testing.expectEqual(Key.left, keys.next().?);
    try std.testing.expectEqual(Key.esc, keys.next().?);
    try std.testing.expectEqual(Key{ .char = '[' }, keys.next().?);
    try std.testing.expectEqual(Key{ .char = 'H' }, keys.next().?);
    try std.testing.expectEqual(Key{ .char = 3 }, keys.next().?);
    try std.testing.expectEqual(Key.esc, keys.next().?);
    try std.testing.expectEqual(Key{ .char = '[' }, keys.next().?);
    try std.testing.expectEqual(@as(?Key, null), keys.next());
}
