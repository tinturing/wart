const std = @import("std");

const wart = @import("wart");

const red = wart.color.Rgb.init(255, 0, 0);
const green = wart.color.Rgb.init(0, 255, 0);
const blue = wart.color.Rgb.init(0, 0, 255);
const yellow = wart.color.Rgb.init(255, 255, 0);

test "wart.image.bytesAsRgb returns slice" {
    var bytes = [_]u8{
        255, 0,   0,
        0,   255, 0,
        0,   0,   255,
        255, 255, 0,
    };

    const actual = try wart.image.bytesAsRgb(bytes[0..]);

    const expected = [4]wart.color.Rgb{
        red, green, blue, yellow,
    };

    try std.testing.expectEqualSlices(wart.color.Rgb, expected[0..], actual);

    const empty = try wart.image.bytesAsRgb(&.{});
    try std.testing.expectEqualSlices(wart.color.Rgb, &.{}, empty);
}

test "wart.image.bytesAsRgb returns Misaligned" {
    var bytes = [_]u8{
        255, 0,   0,
        0,   255, 0,
        0, 0, //0,
    };

    const actual = wart.image.bytesAsRgb(bytes[0..]);

    try std.testing.expectError(wart.image.PixelCastError.Misaligned, actual);
}
