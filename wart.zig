pub const Buffer = buffer.Buffer;
pub const DoubleBuffer = buffer.DoubleBuffer;
pub const Glyph = glyph.Glyph;
pub const Typesetter = @import("src/Typesetter.zig");
pub const Size = matrix.Size;
pub const Chunks = chunks.Chunks;

pub const buffer = @import("src/buffer.zig");
pub const color = @import("src/color.zig");
pub const image = @import("src/image.zig");
pub const matrix = @import("src/matrix.zig");
pub const chunks = @import("src/chunks.zig");
pub const term = @import("src/term.zig");

pub const typesetters = struct {
    pub const asymmetric = @import("src/typesetters/asymmetric.zig").init;
    pub const sextant = @import("src/typesetters/sextant.zig").init;
    pub const quadrant = @import("src/typesetters/quadrant.zig").init;
    pub const half = @import("src/typesetters/half.zig").init;
    pub const block = @import("src/typesetters/block.zig").init;
};

const glyph = @import("src/glyph.zig");
