const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    const mode = b.standardReleaseOptions();
    const target = b.standardTargetOptions(.{});

    const lib = b.addStaticLibrary("wart", "wart.zig");
    lib.setBuildMode(mode);
    lib.install();

    const pkg = std.build.Pkg{
        .name = "wart",
        .source = .{ .path = "wart.zig" },
    };

    {
        const exe = b.addExecutable("show", "examples/show.zig");
        exe.setBuildMode(mode);
        exe.setTarget(target);
        exe.install();
        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const step = b.step("show", "Run the show example");
        step.dependOn(&cmd.step);
    }

    {
        const exe = b.addExecutable("term", "examples/term.zig");
        exe.setBuildMode(mode);
        exe.setTarget(target);
        exe.install();
        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const step = b.step("term", "Run the term example");
        step.dependOn(&cmd.step);
    }

    {
        const exe = b.addExecutable("bench", "examples/bench.zig");
        exe.setBuildMode(mode);
        exe.setTarget(target);
        exe.install();
        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const step = b.step("bench", "Run the bench example");
        step.dependOn(&cmd.step);
    }

    {
        const test_step = b.addTest("test/tests.zig");
        test_step.setBuildMode(mode);
        test_step.addPackage(pkg);

        const step = b.step("test", "Run the test suite");
        step.dependOn(&test_step.step);
    }
}
