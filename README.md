# wart

A library for ASCII/Unicode graphics and TUIs.
Written in [Zig](https://ziglang.org).

## Example

Run the example:

```sh
zig build show -- <typesetter> <image>
```

<typesetter> must be one of: block, half, quadrant, sextant, asymmetric.

The example only supports the Netpbm binary portable pixmap format (P6).

For example:

```sh
zig build show -- asymmetric /path/to/image.ppm
```
